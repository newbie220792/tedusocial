import { Route } from '@core/interface';
import express from 'express';
import mongoose from 'mongoose';
import hpp from 'hpp';
import morgan from 'morgan';
import cors from 'cors';
import helmet from 'helmet';
import { Logger } from '@core/utils';
import errorMiddleware from '@core/middleware/error.middleware';
class App {
    public app: express.Application;
    public port: string | number;
    public production: boolean;

    constructor(routes: Route[]) {
        this.port = 5000;
        this.app = express();
        this.connectMongoose();
        this.production = process.env.NODE_ENV == 'production' ? true : false;
        this.app.use(errorMiddleware);
        this.initializeMiddleWare();
        this.initializeRoutes(routes);
        this.initializeErrorMiddleWare();
    }

    private initializeRoutes(routes: Route[]) {
        routes.forEach((route) => {
            this.app.use("/", route.router);
        })
    }
    public listen() {
        this.app.listen(this.port, () => {
            Logger.info(`Server is running on port ${this.port}`);
        });
    }

    private connectMongoose() {
        const connectString = process.env.MONGODB_URI;
        try {
            if (!connectString) {
                Logger.info("Connect string is invalid....");
                return;
            }
            mongoose.connect(connectString, {
                useNewUrlParser: true,
                useUnifiedTopology: true,
                useFindAndModify: false,
                useCreateIndex: true
            });
            Logger.info("Database connected.....");
        } catch (error) {
            Logger.error('Connect database error.....');
            Logger.error(error);
        }

    }
    // request middleware in front of the route
    private initializeMiddleWare() {
        if (this.production) {
            this.app.use(hpp());
            this.app.use(helmet());
            // this.app.use(morgan('combined'));
            // this.app.use(cors({ origin: true, credentials: true }));
        } else {
            // this.app.use(morgan("dev"));
            // this.app.use(cors({ origin: true, credentials: true }));
        }
        this.app.use(express.json()); // convert body to json
        this.app.use(express.urlencoded({ extended: true }));
    }

    // response middleware in behide the route
    private initializeErrorMiddleWare() {
        this.app.use(errorMiddleware);
    }
}

export default App; 