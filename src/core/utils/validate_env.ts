import { cleanEnv, str } from 'envalid';
// By default, cleanEnv() will log an error message and exit if any required env vars are missing or invalid.
const validateEnv = () => {
    cleanEnv(process.env, {
        NODE_ENV: str(),
        MONGODB_URI: str()
    });
}
export default validateEnv;