import propertiesReader from 'properties-reader';
export default class MessageResource {
    private properties = propertiesReader('./app.properties');
    public getMessage = (key: string): string => {
        return this.properties.get(key) as string;
    }
}