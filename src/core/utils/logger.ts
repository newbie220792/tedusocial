import winston from 'winston';


const myFormat = winston.format.printf(({ level, message, label, timestamp }) => {
    return `${timestamp} [${label}] ${level}: ${message}`;
  });

// logger in console
const Logger: winston.Logger = winston.createLogger({
    transports: [
        // - Write all logs with level `error` and below to `error.log`
        new winston.transports.File({ filename: 'logs/error.log', level: 'error' }),
        // - Write all logs with level `info` and below to `combined.log`
        new winston.transports.File({ filename: 'logs/combined.log', level: 'info' }),],
    format: winston.format.combine(
        winston.format.label({ label: 'Tedusocial!' }),
        winston.format.timestamp({format:'DD-MM-YYYY HH:mm:ss'}),
        myFormat
    )
});

//
// If we're not in production then log to the `console` with the format:
// `${info.level}: ${info.message} JSON.stringify({ ...rest }) `
//
if (process.env.NODE_ENV !== 'production') {
    Logger.add(new winston.transports.Console({
        format: winston.format.simple(),
    }));
}
export default Logger;