import { HttpException } from '@core/exceptions';
import { Request, Response, NextFunction } from 'express';
import { Logger } from '@core/utils';

// middle ware controller error
const errorMiddleware = (error: HttpException, req: Request, res: Response, nextFunction: NextFunction) => {
    const status: number = error.status || 500;
    const message: string = error.message || 'Some thing went wrong';
    res.status(status).send({ status: status, message: message });
}
export default errorMiddleware;