import { IsEmail, IsNotEmpty, MinLength } from "class-validator";

export default class LoginDto {
    @IsNotEmpty()
    @IsEmail()
    public email: string = '';
    @IsNotEmpty()
    @MinLength(6, {
        message: "Password must 6 character!"
    })
    public password: string = '';
    public LoginDto(email: string, password: string) {
        this.email = email;
        this.password = password;
    }

}