import { IsEmail, IsNotEmpty, MinLength } from "class-validator";

export default class RegisterDto {
    @IsNotEmpty()
    public f_name: string | undefined;
    public l_name: string | undefined;
    @IsEmail()
    public email: string | undefined;
    @IsNotEmpty()
    @MinLength(6, {
        message: "Password must 6 character!"
    })
    public password: string | undefined;
}