import { NextFunction, Response, Request } from 'express';
import AuthService from './auth.service';
import LoginDto from '@dtos/authDto/auth.dto';
import { TokenData } from '@modules/auth';
import validationMiddleware from '@core/middleware/validation.middleware';
export default class AuthController {
    private autheService = new AuthService();
    public login = async (req: Request, res: Response, next: NextFunction) => {
        try {
            const model: LoginDto = req.body;
            validationMiddleware(LoginDto, true);
            const tokenData: TokenData = await this.autheService.login(model);
            res.status(200).json({ data: tokenData });
        } catch (error) {
            next(error);
        }
    };
}