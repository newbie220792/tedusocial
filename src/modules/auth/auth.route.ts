import { Route } from "@core/interface";
import { Router } from "express";
import validationMiddleware from '@core/middleware/validation.middleware';
import AuthController from './auth.controller';
import LoginDto from '@dtos/authDto/auth.dto';
export default class AuthRoute implements Route {
    public path = '/api/auth';
    public router = Router();
    private authController = new AuthController();

    constructor() {
        this.initializeRoutes();
    }

    private initializeRoutes() {
        // can use middleware in the route post 
        this.router.post(this.path, this.authController.login);
    }
}