import { HttpException } from "@core/exceptions";
import LoginDto from "@dtos/authDto/auth.dto";
import { isEmpty } from "class-validator";
import UserSchema from '@modules/users/user.model'
import { Logger, MessageResource } from "@core/utils";
import { DataStoredInToken, TokenData } from "./auth.interface";
import bcryptjs from 'bcryptjs';
import IUser from "@modules/users/user.interface";
import jwt from 'jsonwebtoken';

class AuthService {
    public userSchema = UserSchema;
    public messageResource = new MessageResource();
    public async login(model: LoginDto): Promise<TokenData> {
        if (isEmpty(model)) {
            throw new HttpException(400, this.messageResource.getMessage("User.Model.Empty"));
        }
        const user = await this.userSchema.findOne({ email: model.email }) as IUser;
        if (!user) {
            Logger.error(this.messageResource.getMessage("User.Email.Not.Exist"));
            throw new HttpException(409, this.messageResource.getMessage("User.Email.Not.Exist"));
        }
        const isMatchPassword = bcryptjs.compare(model.password!, user.password);
        if (!isMatchPassword) {
            throw new HttpException(400, this.messageResource.getMessage("User.Login.Credential.Invalid"));
        }
        return this.createToken(user);
    }
    private createToken(user: IUser): TokenData {
        const dataInToken: DataStoredInToken = { id: user._id };
        const secret: string = process.env.JWT_TOKEN_SECRET!;
        const expiresIn: number = 3600;
        return {
            token: jwt.sign(dataInToken, secret, { expiresIn: expiresIn }),
        }
    }
}
export default AuthService;