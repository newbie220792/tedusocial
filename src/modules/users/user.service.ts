import UserSchema from './user.model'
import { DataStoredInToken, TokenData } from '../auth/auth.interface';
import { RegisterDto } from '@dtos/UserDto';
import { isEmpty } from '@core/utils/helper';
import { HttpException } from '@core/exceptions';
import gravatar from 'gravatar';
import bcryptjs from 'bcryptjs';
import IUser from './user.interface';
import jwt from 'jsonwebtoken';
import { Logger, MessageResource } from '@core/utils';

class UserService {
    public userSchema = UserSchema;
    public messageResource = new MessageResource();
    public async createUser(model: RegisterDto): Promise<TokenData> {
        if (isEmpty(model)) {
            throw new HttpException(400, this.messageResource.getMessage("User.Model.Empty"));
        }
        const user = await this.userSchema.findOne({ email: model.email });
        if (user) {
            Logger.error(this.messageResource.getMessage("User.Email.Exist"));
            throw new HttpException(409, this.messageResource.getMessage("User.Email.Exist"));
        }

        // create avatar same account google
        const avatar = gravatar.url(model.email!, {
            size: '200',
            rating: 'g',
            default: 'mm'
        });

        const salt = await bcryptjs.genSalt(10);
        const hashedPassword = await bcryptjs.hash(model.password!, salt!);
        const createdUser: IUser = await this.userSchema.create({
            ...model,
            password: hashedPassword,
            avatar: avatar,
            date: '' + Date.now()
        } as IUser)
        return this.createToken(createdUser);
    }

    private createToken(user: IUser): TokenData {
        const dataInToken: DataStoredInToken = { id: user._id };
        const secret: string = process.env.JWT_TOKEN_SECRET!;
        const expiresIn: number = 3600;
        return {
            token: jwt.sign(dataInToken, secret, { expiresIn: expiresIn }),
        }
    }
}

export default UserService;