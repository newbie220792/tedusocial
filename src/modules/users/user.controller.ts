import { RegisterDto } from '@dtos/UserDto';
import { TokenData } from '@modules/auth';
import { NextFunction, Response, Request } from 'express';
import UserService from './user.service';
export default class UserController {
    private userService = new UserService();
    public register = async (req: Request, res: Response, next: NextFunction) => {
        try {
            const model: RegisterDto = req.body;
            const tokenData: TokenData = await this.userService.createUser(model);
            res.status(201).json({ data: tokenData });
        } catch (error) {
            next(error);
        }
    };
}