import { Route } from "@core/interface";
import { Router } from "express";
import UserController from "./user.controller";
import validationMiddleware from '../../core/middleware/validation.middleware';
import RegisterDto from '../../dtos/UserDto/register.dto';
export default class UserRoute implements Route {
    public path = '/api/users';
    public router = Router();
    public userController = new UserController();

    constructor() {
        this.initializeRoutes();
    }

    private initializeRoutes() {
        // can use middleware in the route post 
        validationMiddleware(RegisterDto, true);
        this.router.post(this.path, this.userController.register); // post: http://localhost:5000/api/users
    }
}