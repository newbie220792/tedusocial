import mongoose from 'mongoose';
import IUser from './user.interface';
const UserSchema = new mongoose.Schema({
    f_name: {
        type: String,
        require: true
    },
    l_name: {
        type: String,
        require: true
    },
    email: {
        type: String,
        unique: true,
        index: true,
        require: true
    },
    password: {
        type: String,
        require: true
    },
    avatar: {
        type: String,
    },
    date: {
        type: Date,
        default: Date.now
    },
});
export default mongoose.model<IUser & mongoose.Document>("user", UserSchema);
// Gộp thuộc tính của 2 interfae này vì mongoose.Documnet có nhiều thuộc tính