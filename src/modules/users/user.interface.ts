export default interface IUser {
    _id: string;
    f_name: string;
    email: string;
    l_name: string;
    password: string;
    avatar: string; // use gavatar
    date: string;
}